$(document).ready(function(){

  $('.scroll-to-top').on('click', function() {
    $('html, body').animate({
      scrollTop : 0
    }, 500)
  })

  function sideNavActiveReset() {
    $('.side-nav li').find('.is-active').removeClass()
    if ($('.about-icon-close').hasClass('is-active')) {
      $(this).removeClass('is-active')
    }
  }

  // $('.about-wrap').on('scroll', function() {
    $('.about-us').on('inview', function(event, isInView) {
      if (isInView && $('.about-us').scrollTop() + $('.about-us').height() >= 0) {
        sideNavActiveReset()
        $('.side-nav li:first-child').addClass('is-active')
        $('.about-icon-close').removeClass('is-active')
      } else {
        $('.side-nav li:first-child').removeClass('is-active')
        $('.about-icon-close').addClass('is-active')
      }
    })
    $('.about-facilities').on('inview', function(event, isInView) {
      if (isInView && $('.about-facilities').scrollTop() + $('.about-facilities').height() >= 0) {
        sideNavActiveReset()
        $('.side-nav li:nth-child(2)').addClass('is-active')
        $('.about-icon-close').addClass('is-active')
      } else {
        $('.side-nav li:nth-child(2)').removeClass('is-active')
      }
    })
    $('.about-staff').on('inview', function(event, isInView) {
      if (isInView && $('.about-staff').scrollTop() + $('.about-staff').height() >= 0) {
        sideNavActiveReset()
        $('.side-nav li:last-child').addClass('is-active')
      } else {
        $('.side-nav li:last-child').removeClass('is-active')
      }
    })
  // })

  var targetElement = document.querySelector("body")

  triggerOverlay('.navicon', '.main-menu')
  destroyOverlay('.main-menu .close-btn', '.main-menu')

  triggerOverlay('.about-trigger', '.about-wrap')
  destroyOverlay('.about-wrap .close-btn', '.about-wrap')

  $('.main-menu').on('click', 'a[href*="#"]', function( event ) {
    bodyScrollLock.enableBodyScroll(targetElement)
    $('.main-menu').toggleClass('is-shown')
  })

  function destroyOverlay(selector, triggeredElement) {
    $(selector).on('click', function() {
      bodyScrollLock.enableBodyScroll(targetElement)
      $(triggeredElement).toggleClass('is-shown')
      bodyScrollLock.clearAllBodyScrollLocks()
    })
  }

  function triggerOverlay(selector, triggeredElement) {
    $(selector).on('click', function() {
      bodyScrollLock.disableBodyScroll(targetElement)
      $(triggeredElement).toggleClass('is-shown')
    })
  }

  function animateCSS(element, animationName, callback) {
    var node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
      node.classList.remove('animated', animationName)
      node.removeEventListener('animationend', handleAnimationEnd)

      if (typeof callback === 'function') callback()
    }

  node.addEventListener('animationend', handleAnimationEnd)
}

$('.testimonial-list').slick({
  arrows       : true,
  autoplay     : true,
  autoplaySpeed: 3500,
  speed        : 500,
  fade         : true,
  cssEase      : 'linear',
  pauseOnHover :false,
  prevArrow    :'<button type="button" class="slick-prev"><span class="prev"></span></button>',
  nextArrow    :'<button type="button" class="slick-next"><span class="next"></span></button>'
})

$('.testimonial-list, .team-img-holder').on('setPosition', function () {
  $(this).find('.slick-slide').height('auto')

  var slickTrack = $(this).find('.slick-track')
  var slickTrackHeight = $(slickTrack).height()

  $(this).find('.slick-slide').css('height', slickTrackHeight + 'px')
})

$('.team-img-holder').slick({
  arrows       : false,
  dots         : true,
  speed        : 500,
  fade         : true,
  cssEase      : 'linear',
  appenDots    : 'top'
})

$('.slick-dots li button').html('<span></span>')

$('.team-desc-meta, .team-meta-profile').not('.is-active').css('display', 'none')
$('.slick-dots li').on('click', function() {
  var activeSlide = $(this).parent().siblings().find('.slick-current')

  var slickIndex = activeSlide.data('slick-index')

  $('.team-desc').find('.is-active').fadeOut().removeClass('is-active').addClass('hidden')
  $('.team-desc-meta').siblings('[data-slick-content='+slickIndex+']').removeClass('hidden').fadeIn().addClass('is-active')

})

$('.meta-info').on('click', function() {
  bodyScrollLock.disableBodyScroll(targetElement)
  $('.team-mobile-overlay').addClass('is-shown')

  var activeMember = $(this).addClass('is-active')
  var memberIndex = activeMember.data('team-index')

  $('.team-mobile-overlay').find('.is-active').removeClass('is-active').addClass('hidden')
  $('.team-meta-profile').siblings('[data-profile-index='+memberIndex+']').removeClass('hidden').fadeIn(700).addClass('is-active')
})

$('.team-mobile-overlay .about-icon-close').on('click', function() {
  bodyScrollLock.enableBodyScroll(targetElement)
  $('.team-mobile-overlay').removeClass('is-shown')
  $('.meta-info-holder').find('.is-active').removeClass('is-active')
  $('.team-meta-profile').siblings('.is-active').fadeOut(700)
})

$('.why-offshore').waypoint(function(direction) {
  if (direction == 'down') {
    $('.navbar').removeClass('navbar-aegius')
    $('.navbar').addClass('navbar-mars')
    $('.navbar').addClass('active')
    $('.scroll-to-top').addClass('is-shown')

    $('.navbar .main-nav li').not(':last-child').addClass('hidden')
    $('.menu-label').css('opacity' , 0)
    $('.navbar .scrolling-nav').not(':last-child').removeClass('hidden')
    $('.scrolling-nav li:first-child').addClass('active')
  } else {
    $('.scroll-to-top').removeClass('is-shown')
    $('.navbar').removeClass('navbar-mars')
    $('.navbar').removeClass('active')
    $('.navbar').addClass('navbar-aegius')

    $('.navbar .main-nav li').not(':last-child').removeClass('hidden')
    $('.menu-label').css('opacity' , 1)
    $('.navbar .scrolling-nav').not(':last-child').addClass('hidden')
    $('.scrolling-nav li:first-child').removeClass('active')
  }
}, { offset:'80px' })

$('.why-the-ph').waypoint(function(direction) {
  if (direction == 'down') {
    $('.navbar').removeClass('navbar-mars')
    $('.navbar').addClass('navbar-shadey')
    $('.navbar').addClass('active')
    $('.scrolling-nav li:first-child').removeClass('active')
    $('.scrolling-nav li:nth-child(2)').addClass('active')
  } else {
    $('.navbar').removeClass('navbar-shadey')
    $('.navbar').removeClass('active')
    $('.navbar').addClass('navbar-mars')
    $('.scrolling-nav li:nth-child(2)').removeClass('active')
    $('.scrolling-nav li:first-child').addClass('active')
  }
}, { offset:'80px' })

$('.getting-started').waypoint(function(direction) {
  if (direction == 'down') {
    $('.navbar').removeClass('navbar-shadey')
    $('.navbar').addClass('navbar-mayflower')
    $('.navbar').addClass('active')
    $('.scrolling-nav li:nth-child(2)').removeClass('active')
    $('.scrolling-nav li:nth-child(3)').addClass('active')
  } else {
    $('.navbar').removeClass('navbar-mayflower')
    $('.navbar').removeClass('active')
    $('.navbar').addClass('navbar-shadey')
    $('.scrolling-nav li:nth-child(3)').removeClass('active')
    $('.scrolling-nav li:nth-child(2)').addClass('active')
  }
}, { offset:'80px' })

$('.section-testimonial').waypoint(function(direction) {
  if (direction == 'down') {
    $('.navbar').removeClass('navbar-mayflower')
    $('.navbar').addClass('navbar-shadey')
    $('.navbar').addClass('active')
    $('.scrolling-nav li:nth-child(3)').removeClass('active')
    $('.scrolling-nav li:nth-child(4)').addClass('active')
  } else {
    $('.navbar').removeClass('navbar-shadey')
    $('.navbar').removeClass('active')
    $('.navbar').addClass('navbar-mayflower')
    $('.scrolling-nav li:nth-child(4)').removeClass('active')
    $('.scrolling-nav li:nth-child(3)').addClass('active')
  }
}, { offset:'80px' })

$('.the-team').waypoint(function(direction) {
  if (direction == 'down') {
    $('.navbar').removeClass('navbar-shadey')
    $('.navbar').addClass('navbar-aegius')
    $('.navbar').addClass('active')
    $('.scrolling-nav li:nth-child(4)').removeClass('active')
    $('.scrolling-nav li:last-child').addClass('active')
  } else {
    $('.navbar').removeClass('navbar-aegius')
    $('.navbar').removeClass('active')
    $('.navbar').addClass('navbar-shadey')
    $('.scrolling-nav li:last-child').removeClass('active')
    $('.scrolling-nav li:nth-child(4)').addClass('active')
  }
}, { offset:'40px' })

$('.info-panel .section-btn').on('click', function() {
  $('.quote-panel').removeClass('animated fadeOut')
  $('.info-panel').addClass('animated fadeOut')
  $('.quote-panel').css('height', 'auto')
  $('.quote-panel').addClass('animated fadeIn')
  $('.info-panel').css('height', 0)
})

$('.quote-panel .section-btn').on('click', function() {
  $('.info-panel').removeClass('animated fadeOut')
  $('.quote-panel').addClass('animated fadeOut')
  $('.info-panel').css('height', 'auto')
  $('.info-panel').addClass('animated fadeIn')
  $('.quote-panel').css('height', 0)
})

$('a[href*="#"]')
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
  if (
    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
    &&
    location.hostname == this.hostname
    ) {
    var target = $(this.hash)
  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
  var hashVal = $(this).attr('href').replace('#','')

  if (target.length) {
    event.preventDefault()
    $('.about-wrap').on('scroll', function(){Waypoint.refreshAll();});

    $('.about-wrap').animate({
      scrollTop: $('.about-wrap').scrollTop() + (target.offset().top - $('.about-wrap').offset().top)
    }, 1000)

    if (hashVal === 'about' || hashVal === 'facilities' || hashVal === 'staff') {
      $('.side-nav').find('.is-active').removeClass()
      if (hashVal === 'about') $('.side-nav li:first-child').addClass('is-active')
        else if (hashVal === 'facilities') $('.side-nav li:nth-child(2)').addClass('is-active')
          else if (hashVal === 'staff') $('.side-nav li:last-child').addClass('is-active')
            else return false
          } else {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000)
          }
        }
      }
    })

$('.form-text').focus(function() {
  $(this).parent().addClass('active')
  $('label:before').css('color', 'transparent')
})

$('.form-text').blur(function() {
  $(this).parent().removeClass('active')
})

$('.contact-form-btn').on('click', function() {
  var mainMenu = $('.main-menu')

  if (mainMenu.hasClass('is-shown')) {
    $('.main-menu').removeClass('is-shown')
    bodyScrollLock.clearAllBodyScrollLocks()
  }

  bodyScrollLock.disableBodyScroll(targetElement)
  $('.contact-overlay').addClass('is-shown')
  setTimeout(function(){
    $('.contact-wrap').addClass('is-shown')
  }, 400)
})

$(document).on('keyup',function(evt) {
  if ($('.contact-wrap').is('.is-shown') && $('.contact-overlay').is('.is-shown')) {
    if (evt.keyCode == 27) {
      bodyScrollLock.enableBodyScroll(targetElement)
      setTimeout(function(){
        $('.contact-wrap').removeClass('is-shown')
      }, 400)
      $('.contact-overlay').removeClass('is-shown')
    }
  }
})

$('.contact-wrap .close-btn, .contact-overlay').on('click', function() {
  bodyScrollLock.enableBodyScroll(targetElement)
  setTimeout(function(){
    $('.contact-wrap').removeClass('is-shown')
  }, 400)
  $('.contact-overlay').removeClass('is-shown')
})


$('.form-two > .form-one-trigger').on('click', function() {
  $('.form-two').toggleClass('hide-element')
  $('.form-one').toggleClass('hide-element')
})

$('.form-one > .form-one-trigger').on('click', function(e  ) {
  e.preventDefault()
  $('.form-one :input').each(function(){
    if (isNullOrWhitespace($(this).val())) {
      $(this).addClass('input-error')
    } else {
      $(this).removeClass('input-error')
      if ($('.form-one input[type=email]')) {
        if (!validateEmail($('.form-one input[type=email]').val())) {
          $('.form-one input[type=email]').addClass('input-error')
        } else {
          $('.form-one input[type=email]').removeClass('input-error')
          $('.form-two').toggleClass('hide-element')
          $('.form-one').toggleClass('hide-element')
        }
      }
    }
  })
})

$('.form-two textarea').on('blur', function() {
  if (isNullOrWhitespace($(this).val())) {
    $(this).addClass('input-error')
  } else {
    $(this).removeClass('input-error')
    $('.form-two > .form-two-send').removeClass('btn-disabled')
  }
})

$('.form-two textarea').on('keyup', function() {
  if (isNullOrWhitespace($(this).val())) {
    $('.form-two > .form-two-send').addClass('btn-disabled')
    $(this).addClass('input-error')
  } else {
    $(this).removeClass('input-error')
    $('.form-two > .form-two-send').removeClass('btn-disabled')
  }
})

function validateEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/
  return regex.test(email)
}

function isNullOrWhitespace(input) {
  return input === null || input.match(/^ *$/) !== null
}

})
