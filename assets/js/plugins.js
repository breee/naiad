// Avoid `console` errors in browsers that lack a console.
$(function() {
  var method;
  var noop = function () {};
  var methods = [
  'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
  'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
  'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
  'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
          console[method] = noop;
        }
      }
    }())

// Place any jQuery/helper plugins in here

$('.list-container').matchHeight({
  target: $('.height-target')
})

var notoSansBlack   = new FontFaceObserver('NotoSans Black'),
    notoSansBold    = new FontFaceObserver('NotoSans Bold'),
    notoSansMedium  = new FontFaceObserver('NotoSans Medium'),
    notoSansRegular = new FontFaceObserver('NotoSans Regular'),
    notoSansLight   = new FontFaceObserver('NotoSans Light'),
    notoSansThin    = new FontFaceObserver('NotoSans Thin')

Promise.all([
  notoSansBlack.load(),
  notoSansBold.load(),
  notoSansMedium.load(),
  notoSansRegular.load(),
  notoSansLight.load(),
  notoSansThin.load()])
    .then(function () {
  console.log('Fonts loaded')
})
